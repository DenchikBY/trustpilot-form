<?php declare(strict_types=1);

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Routing\RouterInterface;

abstract class FunctionalTestCase extends WebTestCase
{
    /** @var KernelBrowser */
    protected $client;

    /** @var RouterInterface */
    protected $router;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->router = static::$container->get('router');
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown(): void
    {
        $this->router = null;
        $this->client = null;

        parent::tearDown();
    }
}

<?php declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\FunctionalTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\UserInfoFormController
 */
class UserInfoFormControllerTest extends FunctionalTestCase
{
    /**
     * @covers \App\Controller\UserInfoFormController::main
     */
    public function testMainView(): void
    {
        $url = $this->router->generate('info_form_main');

        $this->client->request(Request::METHOD_GET, $url);
        $response = $this->client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @covers \App\Controller\UserInfoFormController::main
     */
    public function testMainValidation(): void
    {
        $url = $this->router->generate('info_form_main');

        $this->client->request(Request::METHOD_POST, $url, [
            'user_info' => [
                'firstName' => '',
                'lastName'  => '',
                'email'     => '',
                'address'   => '',
            ],
        ]);
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertContains('This value should not be blank.', $content);
    }

    /**
     * @covers \App\Controller\UserInfoFormController::main
     */
    public function testMainSend(): void
    {
        $url = $this->router->generate('info_form_main');

        $this->client->request(Request::METHOD_POST, $url, [
            'user_info' => [
                'firstName' => 'First Name',
                'lastName'  => 'Last Name',
                'email'     => 'test@test.com',
                'address'   => 'Address',
            ],
        ]);
        /** @var RedirectResponse $response */
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertInstanceOf(RedirectResponse::class, $response);

        $this->client->request(Request::METHOD_GET, $response->getTargetUrl());
        $response = $this->client->getResponse();
        $content  = $response->getContent();

        $this->assertContains('Sent successfully', $content);
    }
}

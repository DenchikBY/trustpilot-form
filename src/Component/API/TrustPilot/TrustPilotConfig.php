<?php declare(strict_types=1);
/**
 * This file is subject to the terms and conditions defined in file 'LICENSE', which is part of this source code
 * package. If the file is missing a copy can be found at:
 * https://gitlab.cybercoder.site/vj/policies-procedures-standards/blob/master/licensing/GHCM-LICENSE.
 */

namespace App\Component\API\TrustPilot;

class TrustPilotConfig
{
    /** @var string */
    private $apiUrl;

    /** @var string */
    private $businessUnitId;

    /** @var string */
    private $apiKey;

    /** @var string */
    private $apiSecret;

    /** @var string */
    private $apiUsername;

    /** @var string */
    private $apiPassword;

    /**
     * @param string $apiUrl
     * @param string $businessUnitId
     * @param string $apiKey
     * @param string $apiSecret
     * @param string $apiUsername
     * @param string $apiPassword
     */
    public function __construct(
        string $apiUrl,
        string $businessUnitId,
        string $apiKey,
        string $apiSecret,
        string $apiUsername,
        string $apiPassword
    ) {
        $this->apiUrl         = $apiUrl;
        $this->businessUnitId = $businessUnitId;
        $this->apiKey         = $apiKey;
        $this->apiSecret      = $apiSecret;
        $this->apiUsername    = $apiUsername;
        $this->apiPassword    = $apiPassword;
    }

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return $this->apiUrl;
    }

    /**
     * @return string
     */
    public function getBusinessUnitId(): string
    {
        return $this->businessUnitId;
    }

    /**
     * @return string
     */
    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getApiSecret(): string
    {
        return $this->apiSecret;
    }

    /**
     * @return string
     */
    public function getApiUsername(): string
    {
        return $this->apiUsername;
    }

    /**
     * @return string
     */
    public function getApiPassword(): string
    {
        return $this->apiPassword;
    }
}

<?php declare(strict_types=1);

namespace App\Component\API\TrustPilot;

use App\DTO\UserInfo;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\ResponseInterface;

class FakeTrustPilotClient implements TrustPilotClientInterface
{
    /**
     * Fake client which always return successful response and bad request for special name.
     *
     * {@inheritDoc}
     */
    public function createInvitation(UserInfo $userInfo): ResponseInterface
    {
        return new MockResponse('', [
            'http_code' => 'John Doe' === $userInfo->getFullName()
                ? Response::HTTP_BAD_REQUEST
                : Response::HTTP_OK,
        ]);
    }
}

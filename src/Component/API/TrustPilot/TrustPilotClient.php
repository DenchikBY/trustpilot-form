<?php declare(strict_types=1);

namespace App\Component\API\TrustPilot;

use App\DTO\UserInfo;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * TODO not tested
 */
class TrustPilotClient implements TrustPilotClientInterface
{
    private const CACHE_ITEM_TOKEN = 'trust.pilot.token';

    /** @var HttpClientInterface */
    private $httpClient;

    /** @var AdapterInterface */
    private $cache;

    /** @var TrustPilotConfig */
    private $config;

    /**
     * @param HttpClientInterface $httpClient
     * @param AdapterInterface    $cache
     * @param TrustPilotConfig    $config
     */
    public function __construct(
        HttpClientInterface $httpClient,
        AdapterInterface $cache,
        TrustPilotConfig $config
    ) {
        $this->httpClient = $httpClient;
        $this->cache      = $cache;
        $this->config     = $config;
    }

    /**
     * {@inheritDoc}
     *
     * @param UserInfo $userInfo
     *
     * @throws TransportExceptionInterface
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     *
     * @return ResponseInterface
     */
    public function createInvitation(UserInfo $userInfo): ResponseInterface
    {
        $url = "/private/business-units/{$this->config->getBusinessUnitId()}/email-invitations";

        return $this->makeRequest(Request::METHOD_POST, $url, [
            'consumerEmail'           => $userInfo->email,
            'replyTo'                 => $userInfo->email,
            'referenceNumber'         => 'inv00001',
            'consumerName'            => $userInfo->getFullName(),
            'locale'                  => 'en-US',
            'senderEmail'             => $userInfo->email,
            'serviceReviewInvitation' => [
                'preferredSendTime' => (new \DateTime())->format('Y-m-d\TH:i:s'),
                'redirectUri'       => 'http://trustpilot.com',
                'tags'              => ['tag 1', 'tag 2'],
                'templateId'        => '507f191e810c19729de860ea',
            ],
            'locationId'              => 'ABC123',
            'senderName'              => $userInfo->getFullName(),
        ]);
    }

    /**
     * @param string $method
     * @param string $url
     * @param array  $data
     *
     * @throws TransportExceptionInterface
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     *
     * @return ResponseInterface
     */
    private function makeRequest(string $method, string $url, array $data): ResponseInterface
    {
        $response = $this->httpClient->request($method, $this->config->getApiUrl() . $url, [
            'auth_bearer' => $this->getToken(),
            'json'        => $data,
        ]);

        if (Response::HTTP_UNAUTHORIZED === $response->getStatusCode()) {
            $response = $this->httpClient->request($method, $this->config->getApiUrl() . $url, [
                'auth_bearer' => $this->getToken(true),
                'json'        => $data,
            ]);
        }

        return $response;
    }

    /**
     * Get token from cache if exists, or get from API and save to cache.
     *
     * @param bool $forceNew
     *
     * @throws TransportExceptionInterface
     * @throws \Psr\Cache\InvalidArgumentException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     *
     * @return string
     */
    private function getToken(bool $forceNew = false): string
    {
        $item = $this->cache->getItem(self::CACHE_ITEM_TOKEN);

        if ($forceNew || $item->isHit()) {
            $this->cache->save(
                $item->set($this->getNewToken())
            );
        }

        return $item->get();
    }

    /**
     * Get token from API.
     *
     * @throws TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     *
     * @return string
     */
    private function getNewToken(): string
    {
        $url = $this->config->getApiUrl() . '/oauth/oauth-business-users-for-applications/accesstoken';

        $response = $this->httpClient->request(Request::METHOD_GET, $url, [
            'auth_basic' => \base64_encode($this->config->getApiKey() . ':' . $this->config->getApiSecret()),
            'query'      => [
                'grant_type' => 'password',
                'username'   => $this->config->getApiUsername(),
                'password'   => $this->config->getApiPassword(),
            ],
        ]);

        if (Response::HTTP_OK !== $response->getStatusCode()) {
            throw new HttpException($response->getStatusCode());
        }

        $content = $response->toArray();

        if (!isset($content['access_token']) || empty($content['access_token'])) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED);
        }

        return $content['access_token'];
    }
}

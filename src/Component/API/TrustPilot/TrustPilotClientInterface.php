<?php declare(strict_types=1);

namespace App\Component\API\TrustPilot;

use App\DTO\UserInfo;
use Symfony\Contracts\HttpClient\ResponseInterface;

interface TrustPilotClientInterface
{
    /**
     * @param UserInfo $userInfo
     *
     * @return ResponseInterface
     */
    public function createInvitation(UserInfo $userInfo): ResponseInterface;
}

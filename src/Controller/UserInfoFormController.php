<?php declare(strict_types=1);

namespace App\Controller;

use App\Component\API\TrustPilot\TrustPilotClientInterface;
use App\DTO\UserInfo;
use App\Form\Type\UserInfoFormType;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * @Route("", name="info_form_")
 */
class UserInfoFormController extends AbstractController
{
    /**
     * @Route("", name="main")
     *
     * @param Request                   $request
     * @param TrustPilotClientInterface $trustPilotClient
     * @param LoggerInterface           $logger
     *
     * @throws TransportExceptionInterface
     *
     * @return Response
     */
    public function main(
        Request $request,
        TrustPilotClientInterface $trustPilotClient,
        LoggerInterface $logger
    ): Response {
        /** @var Form $form */
        $form = $this
            ->createForm(UserInfoFormType::class)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UserInfo $userInfo */
            $userInfo = $form->getData();

            try {
                $apiResponse = $trustPilotClient->createInvitation($userInfo);
            } catch (\Throwable $exception) {
                $logger->error($exception->getMessage(), ['context' => $exception]);

                $apiResponse = null;
            }

            if ($apiResponse instanceof ResponseInterface && Response::HTTP_OK === $apiResponse->getStatusCode()) {
                $this->addFlash('success', 'Sent successfully');

                return $this->redirect($request->getUri());
            }

            $this->addFlash('danger', 'Error during send via API');
        }

        return $this->render('UserInfoForm/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

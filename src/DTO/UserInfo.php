<?php declare(strict_types=1);

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class UserInfo
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $firstName;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $lastName;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Email
     */
    public $email;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $address;

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }
}
